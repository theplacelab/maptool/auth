const { v4: uuidv4 } = require('uuid');
const bcrypt = require('bcrypt');
const { faker } = require('@faker-js/faker');
const { ROLE } = require('../../constants.js');

module.exports = {
  testUsers: [
    {
      id: 0,
      avatar: faker.internet.avatar(),
      name: faker.person.fullName(),
      email: faker.internet.email().toLowerCase(),
      password: faker.internet.password(),
      role: ROLE.USER,
      uuid: uuidv4()
    },
    {
      id: 1,
      avatar: faker.internet.avatar(),
      name: faker.person.fullName(),
      email: faker.internet.email().toLowerCase(),
      password: faker.internet.password(),
      role: ROLE.USER,
      uuid: uuidv4()
    },
    {
      id: 2,
      avatar: faker.internet.avatar(),
      name: faker.person.fullName(),
      email: faker.internet.email().toLowerCase(),
      password: faker.internet.password(),
      role: ROLE.SUPER,
      uuid: uuidv4()
    }
  ],
  seed: function (knex) {
    const saltRounds = 10;
    return knex('users')
      .del()
      .then(async function () {
        let seedUsers = [];
        const promises = [];
        for (let idx = 0; idx < module.exports.testUsers.length; idx++) {
          const u = module.exports.testUsers[idx];
          promises.push(
            bcrypt.hash(u.password, saltRounds).then((password) => {
              const seedUser = {
                ...u,

                password,
                f_reset: false,
                f_valid: true,
                created_at: new Date().toISOString(),
                last_access: new Date().toISOString()
              };
              seedUsers.push(seedUser);
            })
          );
        }
        await Promise.all(promises);
        await knex('users').insert(seedUsers);
      });
  }
};
