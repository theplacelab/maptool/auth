// Note: You don't need to specify an ID column, it will be added automatically
// prettier-ignore
module.exports = {
  tables:{
    users: {
      email:          { type: "text",     maxlength: 2000,  unique: true,  nullable: true, validations: {isEmail: true}},
      name:           { type: "text",     maxlength: 256,   nullable: true },
      password:       { type: "text",     maxlength: 2000,  nullable: true, validations: {isEmail: true}},
      role:           { type: "text",     maxlength: 64,    nullable: true },
      uuid:           { type: "string",   maxlength: 36,    unique: true,    nullable: false, validations: {isUUID: true}},
      avatar:         { type: "text",     maxlength: 2000,  nullable: true },
      f_reset:        { type: "boolean",  nullable: false },
      f_valid:        { type: "boolean",  nullable: false },
      refresh_secret: { type: "string",   maxlength: 2000,  nullable: true, validations: {isUUID: true}},
      created_at:     { type: "dateTime", nullable: false },
      last_access:    { type: "dateTime", nullable: true },
    }
  }
};
