const { RESPONSE } = require('../constants.js');
const schema = require('./schema.js');

module.exports = {
  parseNestedJson: (data) => {
    const _jsonParseNofail = (data) => {
      try {
        return JSON.parse(data);
      } catch {
        return data;
      }
    };
    const _parse = (item) => {
      const parsed = {};
      if (item)
        Object.keys(item).forEach(
          (key) => (parsed[key] = _jsonParseNofail(item[key]))
        );
      return parsed;
    };
    if (Array.isArray(data)) {
      return data.map((item) => _parse(item));
    } else {
      return _parse(data);
    }
  },
  malformed: ({ data, error, msg = 'Malformed Request' }) => {
    console.log(error);
    return {
      msg,
      code: RESPONSE.BAD_REQUEST,
      data: { error, sent: data }
    };
  },
  unauthorized: ({ error, data, msg = 'Not Authorized' }) => ({
    msg,
    code: RESPONSE.NOT_AUTHORIZED,
    data: { error, sent: data }
  }),
  conflict: ({ error, data, msg = 'Conflict' }) => ({
    msg,
    code: RESPONSE.CONFLICT,
    data: { error, sent: data }
  }),
  created: ({ data, msg = 'Created' }) => ({
    msg,
    code: RESPONSE.CREATED,
    data: module.exports.parseNestedJson(data)
  }),
  notFound: ({ error, data, msg = 'Not Found' }) => ({
    msg,
    code: RESPONSE.NOT_FOUND,
    data: { error, sent: data }
  }),
  ok: ({ data, msg = 'Ok!' }) => ({
    msg,
    code: RESPONSE.OK,
    data: module.exports.parseNestedJson(data)
  }),
  createTable: (knex, tableName) => {
    const tableDef = schema.tables[tableName];
    return knex.schema.createTable(tableName, (table) => {
      console.log(`DB: CREATING TABLE: ${tableName}`);
      Object.keys(tableDef).forEach((rowName) => {
        const row = tableDef[rowName];
        if (row.primary) table.primary(rowName);
        if (row.nullable) {
          if (row.unique) {
            table[row.type](rowName, row.maxlength).unique().nullable();
          } else {
            table[row.type](rowName, row.maxlength).nullable();
          }
        } else {
          if (row.unique) {
            table[row.type](rowName, row.maxlength).unique().notNullable();
          } else {
            table[row.type](rowName, row.maxlength).notNullable();
          }
        }
      });
      table.increments();
    });
  }
};
