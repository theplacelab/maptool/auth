global.console = {
  log: jest.fn(),
  error: console.error,
  debug: console.debug,
  trace: console.trace
};
