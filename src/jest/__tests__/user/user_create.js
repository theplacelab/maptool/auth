const request = require('supertest');
const { mock } = require('nodemailer');
const server = require('../../../app');
const {
  knex,
  before,
  after,
  generateUser,
  validTokenFor
} = require('../common.js');
const { testUsers } = require('../../../data/seeds/01_test_users');
const { RESPONSE, ROLE } = require('../../../constants.js');

beforeAll(before);
afterAll(after);

// Create
describe(`API: Create User`, () => {
  test('ANON: Create should return the new webuser and email a validation link', (done) => {
    server.start(
      () => {
        const testUser = { ...generateUser(), role: ROLE.USER };
        request(server.app)
          .put(`/user`)
          .send(testUser)
          .end((err, res) => {
            if (err) throw err;
            const { role, email, name, f_reset, f_valid } = JSON.parse(
              res.text
            );
            expect(res.statusCode).toEqual(RESPONSE.CREATED);
            expect(email).toEqual(testUser.email.toLowerCase());
            expect(name).toEqual(testUser.name);
            expect(role).toEqual(ROLE.USER);
            expect(f_reset).toBeTruthy();
            expect(f_valid).toBeFalsy();
            mock.onSend = () => {
              const sentEmails = mock.getSentMail();
              expect(sentEmails[0].to).toBe(email);
              done();
            };
          });
      },
      knex,
      true
    );
  });

  test('ANON: Malformed data should return bad request', (done) => {
    server.start(
      () => {
        request(server.app)
          .put(`/user`)
          .send({ garbage: 'in', nothing: 'useful' })
          .end((err, res) => {
            if (err) throw err;
            expect(res.statusCode).toEqual(RESPONSE.BAD_REQUEST);
            done();
          });
      },
      knex,
      true
    );
  });

  test('ANON: Duplicate CREATE should return a conflict', (done) => {
    server.start(
      () => {
        const testUser = generateUser({ role: ROLE.USER });
        request(server.app)
          .put(`/user`)
          .send(testUser)
          .end((err, res) => {
            if (err) throw err;
            expect(res.statusCode).toEqual(RESPONSE.CREATED);
            request(server.app)
              .put(`/user`)
              .send(testUser)
              .end((err, res) => {
                if (err) throw err;
                expect(res.statusCode).toEqual(409);
                done();
              });
            done();
          });
      },
      knex,
      true
    );
  });

  test('SUPER: Superusers can directly create active superusers', (done) => {
    const superUser = generateUser({ role: ROLE.SUPER });
    const targetUser = generateUser({ role: ROLE.SUPER });
    server.start(
      () => {
        request(server.app)
          .put(`/user`)
          .send(targetUser)
          .set('Authorization', `bearer ${validTokenFor(superUser)}`)
          .end((err, res) => {
            if (err) throw err;
            const { role, email, name, f_reset, f_valid } = JSON.parse(
              res.text
            );

            expect(res.statusCode).toEqual(RESPONSE.CREATED);
            expect(role).toEqual(ROLE.SUPER);

            expect(email).toEqual(targetUser.email.toLowerCase());
            expect(name).toEqual(targetUser.name);
            expect(f_reset).toBeFalsy();
            expect(f_valid).toBeTruthy();
            done();
          });
      },
      knex,
      true
    );
  });

  test('USER: Regular users cannot directly create users', (done) => {
    const targetUser = generateUser({ role: ROLE.SUPER });
    server.start(
      () => {
        request(server.app)
          .put(`/user`)
          .send(targetUser)
          .set(
            'Authorization',
            `bearer ${validTokenFor(generateUser({ role: ROLE.USER }))}`
          )
          .end((err, res) => {
            if (err) throw err;
            expect(res.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
            done();
          });
      },
      knex,
      true
    );
  });
});
