const request = require('supertest');
const server = require('../../../app');
const { knex, before, after } = require('../common.js');
const { testUsers } = require('../../../data/seeds/01_test_users');
const { faker } = require('@faker-js/faker');
const jwt = require('jsonwebtoken');
const { RESPONSE, ROLE } = require('../../../constants.js');

beforeAll(before);
afterAll(after);

describe(`API: Login`, () => {
  test('Valid login should return a valid token', (done) => {
    const testUser = testUsers.find((user) => user.role === ROLE.USER);
    server.start(
      () => {
        request(server.app)
          .post(`/login`)
          .send({
            email: testUser.email,
            pass: testUser.password
          })
          .then((response) => {
            const token = JSON.parse(response.text).auth;
            jwt.verify(
              token,
              process.env.JWT_SECRET,
              { ignoreExpiration: false },
              (err, decodedToken) => {
                expect(decodedToken.name).toEqual(testUser.name);
                expect(decodedToken.uuid).toEqual(testUser.uuid);
                if (err) done();
              }
            );
            done();
          });
      },
      knex,
      true
    );
  });
  test('Wrong password should return unauthorized', (done) => {
    const testUser = testUsers.find((user) => user.role === ROLE.USER);
    server.start(
      () => {
        request(server.app)
          .post(`/login`)
          .send({
            email: testUser.email,
            pass: faker.internet.password()
          })
          .then((response) => {
            expect(response.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
            done();
          });
      },
      knex,
      true
    );
  });
  test('Malformed request should return unauthorized', (done) => {
    server.start(
      () => {
        request(server.app)
          .post(`/login`)
          .send({
            garbage: 'blah',
            password: faker.internet.password()
          })
          .then((response) => {
            expect(response.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
            done();
          });
      },
      knex,
      true
    );
  });
});
