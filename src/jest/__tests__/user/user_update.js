const request = require('supertest');
const server = require('../../../app');
const {
  knex,
  before,
  after,
  generateUser,
  validTokenFor
} = require('../common.js');
const { testUsers } = require('../../../data/seeds/01_test_users');
const { faker } = require('@faker-js/faker');
const { v4: uuidv4 } = require('uuid');
const { ROLE, RESPONSE } = require('../../../constants.js');

beforeAll(before);
afterAll(after);

const _jsonParseNofail = (data) => {
  try {
    return JSON.parse(data);
  } catch {
    return data;
  }
};

const _testUpdate = ({
  requester,
  target,
  update,
  done,
  expectations,
  alsoExpect
}) => {
  server.start(
    () => {
      knex('users')
        .insert(target)
        .then(([newId]) => {
          knex
            .select('*')
            .from('users')
            .where({ id: newId })
            .then(([newUser]) => {
              request(server.app)
                .patch(`/user/${newUser.uuid}`)
                .send(update)
                .set('Authorization', `bearer ${validTokenFor(requester)}`)
                .end((err, res) => {
                  if (err) throw err;
                  const updatedUser = _jsonParseNofail(res.text);
                  if (expectations) {
                    expectations({ res, updatedUser, target });
                    done();
                  } else {
                    expect(res.statusCode).toEqual(RESPONSE.OK);
                    expect(updatedUser.name).toEqual(update.name);
                    expect(updatedUser.email).toEqual(
                      update.email.toLowerCase()
                    );
                    expect(updatedUser.avatar).toEqual(update.avatar);
                    expect(updatedUser.uuid).toEqual(target.uuid);
                    expect(updatedUser.created_at).toEqual(target.created_at);
                    if (typeof alsoExpect === 'function')
                      alsoExpect({ updatedUser, target });
                    done();
                  }
                });
            });
        });
    },
    knex,
    true
  );
};

describe(`API: Update User`, () => {
  test('ANON: Update should fail', (done) => {
    server.start(
      () => {
        request(server.app)
          .patch(`/user/1`)
          .send({})
          .end((err, res) => {
            if (err) throw err;
            expect(res.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
            done();
          });
      },
      knex,
      true
    );
  });

  test('SUPER: Update self should return selectively modified record', (done) => {
    const target = generateUser({ f_valid: true, role: ROLE.SUPER });
    _testUpdate({
      target,
      requester: target,
      update: {
        name: faker.person.fullName(),
        email: faker.internet.email(),
        avatar: faker.internet.avatar(),
        id: 10,
        uuid: uuidv4(),
        role: ROLE.SUPER,
        f_reset: false,
        f_valid: true,
        password: '123',
        created_at: new Date().toISOString()
      },
      done
    });
  });

  test('SUPER: Update other should return selectively modified record', (done) => {
    _testUpdate({
      requester: generateUser({ f_valid: true, role: ROLE.SUPER }),
      target: generateUser({ f_valid: true, role: ROLE.USER }),
      update: {
        name: faker.person.fullName(),
        email: faker.internet.email(),
        avatar: `${faker.internet.url()}/avatar.png`,
        id: 10,
        uuid: uuidv4(),
        role: ROLE.SUPER,
        f_reset: false,
        f_valid: true,
        password: '123',
        created_at: new Date().toISOString()
      },
      done
    });
  });

  test('SUPER: Can change users role', (done) => {
    const target = generateUser({ f_valid: true, role: ROLE.USER });
    _testUpdate({
      requester: generateUser({ f_valid: true, role: ROLE.SUPER }),
      target,
      update: {
        ...target,
        role: ROLE.SUPER
      },
      alsoExpect: ({ updatedUser }) => {
        expect(updatedUser.role).toEqual(ROLE.SUPER);
      },
      done
    });
  });

  test('USER: Cannot change own role', (done) => {
    const requester = generateUser({ f_valid: true, role: ROLE.USER });
    _testUpdate({
      requester,
      target: requester,
      update: {
        ...requester,
        role: ROLE.SUPER
      },
      expectations: ({ res, updatedUser }) => {
        expect(res.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
      },
      done
    });
  });

  test('USER: Cannot change another users role', (done) => {
    const requester = generateUser({ f_valid: true, role: ROLE.USER });
    const target = generateUser({ f_valid: true, role: ROLE.USER });
    _testUpdate({
      requester,
      target,
      update: {
        ...target,
        role: ROLE.SUPER
      },
      expectations: ({ res }) => {
        expect(res.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
      },
      done
    });
  });

  test('USER: Cannot modify another user', (done) => {
    const requester = generateUser({ f_valid: true, role: ROLE.USER });
    const target = generateUser({ f_valid: true, role: ROLE.USER });
    _testUpdate({
      requester,
      target,
      update: {
        ...target,
        email: 'doggo@mcdoggersons.com'
      },
      expectations: ({ res }) => {
        expect(res.statusCode).toEqual(RESPONSE.NOT_AUTHORIZED);
      },
      done
    });
  });

  test('USER: Update self should only succeed if account is validated', (done) => {
    const target = generateUser({ f_valid: true, role: ROLE.USER });
    _testUpdate({
      requester: target,
      target,
      update: {
        name: faker.person.fullName(),
        email: faker.internet.email(),
        avatar: `${faker.internet.url()}/avatar.png`,
        id: 10,
        uuid: uuidv4(),
        f_reset: false,
        f_valid: true,
        password: '123',
        created_at: new Date().toISOString()
      },
      done
    });
  });

  test('USER: Update self email should only be allowed if email is not already in use', (done) => {
    const target = generateUser({ f_valid: true, role: ROLE.USER });
    _testUpdate({
      requester: target,
      target,
      update: {
        email: testUsers[0].email
      },
      expectations: ({ res }) => {
        expect(res.statusCode).toEqual(RESPONSE.CONFLICT);
      },
      done
    });
  });

  test('USER: Update self email should only be allowed if email is not already in use', (done) => {
    const target = generateUser({ f_valid: true, role: ROLE.USER });
    _testUpdate({
      requester: target,
      target,
      update: {
        email: faker.internet.email()
      },
      expectations: ({ res }) => {
        expect(res.statusCode).toEqual(RESPONSE.OK);
      },
      done
    });
  });
});
