'use strict';
require('dotenv').config({ path: require('find-config')('.env') });
const express = require('express');
const cors = require('cors');
const debug = require('./modules/debugLog.js');
const env = require('./modules/environment.js');
const bodyParser = require('body-parser');
const base = require('./routes/base');
const user = require('./routes/user');
const users = require('./routes/users');
const mail = require('./routes/mail');
const login = require('./routes/login');
const reauthorize = require('./routes/reauthorize');
const knexConfig = require('./data/knexfile.js');
const { RESPONSE } = require('./constants.js');
const app = express();

module.exports = {
  app,
  start: (cb, overrideKnex, shouldSeed = false) => {
    const knex = overrideKnex ? overrideKnex : require('knex')(knexConfig);
    if (env.validate()) {
      knex.migrate.latest().then(() => {
        const configApi = () => {
          app.engine('html', require('ejs').renderFile);
          app.use(cors());
          app.use(bodyParser.json());
          app.use((err, _req, res, next) => {
            if (!err) next();
            console.error(err);
            res.status(RESPONSE.UNPROCESSABLE).send({ error: 'Invalid JSON' });
          });
          app.use((req, _res, next) => {
            req.knex = knex;
            next();
          });
          app.use('/login', login);
          app.use('/reauthorize', reauthorize);
          app.use('/', debug);
          app.use('/', base);
          app.use('/user', user);
          app.use('/users', users);
          app.use('/mail', mail);
          app.use((req, res) => {
            debug.log(`REQ: 404 - No Route`);
            res.sendStatus(RESPONSE.NOT_FOUND);
          });
          env.display();
          if (typeof cb === 'function') cb(knex);
        };
        if (shouldSeed) {
          knex.seed.run().then(async () => {
            const users = await knex.select('*').from('users');
            configApi();
          });
        } else {
          configApi();
        }
      });
    }
  }
};
