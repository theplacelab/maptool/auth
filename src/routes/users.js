const router = require('express').Router();
const user = require('../data/models/user');
const { requireMinimumRole } = require('../modules/validation');
const { RESPONSE, ROLE } = require('../constants.js');

router.get('/', (req, res) => {
  requireMinimumRole(ROLE.SUPER, req, res, () =>
    user.read(req.knex, { uuid: '*' }, (data) =>
      res.status(RESPONSE.OK).json(data)
    )
  );
});

module.exports = router;
