const router = require('express').Router();
const {
  requireMinimumRoleOrSelf,
  generateValidationLinkFor,
  requireValidPostedValidationToken,
  requireInBody,
  requireInParams,
  isSecureRequest,
  requireMinimumRole
} = require('../modules/validation');
const user = require('../data/models/user');
const emailer = require('../modules/emailer/index.js');
const { TEMPLATE } = require('../modules/emailer/index.js');
const generator = require('generate-password');
const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');
const mgTransport = require('nodemailer-mailgun-transport');
const { RESPONSE, ROLE } = require('../constants.js');

const transporter = process.env.DEBUG
  ? nodemailer.createTransport({
      host: 'smtp.mailtrap.io',
      port: 2525,
      auth: {
        user: process.env.MAIL_MAILTRAP_USER,
        pass: process.env.MAIL_MAILTRAP_PASS
      },
      pool: true,
      rateDelta: 10000,
      rateLimit: 1,
      maxConnections: 1
    })
  : (process.env.DEBUG
      ? 'MAILTRAP'
      : process.env.MAIL_MAILGUN_API_KEY
      ? 'MAILGUN'
      : 'SENDGRID') === 'SENDGRID'
  ? nodemailer.createTransport(
      sgTransport({
        auth: {
          api_key: process.env.MAIL_SENDGRID_API_KEY
        },
        pool: true,
        rateDelta: 10000,
        rateLimit: 1,
        maxConnections: 1
      })
    )
  : nodemailer.createTransport(
      mgTransport({
        auth: {
          api_key: process.env.MAIL_MAILGUN_API_KEY,
          domain: process.env.MAIL_MAILGUN_DOMAIN
        }
      })
    );

const sendValidationEmailTo = (knex, to, newPass) => {
  generateValidationLinkFor(knex, to).then((validationLink) => {
    emailer.sendTo(transporter, {
      template: TEMPLATE.VALIDATION,
      to,
      content: {
        newPass,
        validationLink
      }
    });
  });
};

router.put('/', (req, res) => {
  requireInBody(['name', 'email'], req, res, () => {
    const newPass = generator.generate({
      length: 20,
      numbers: true
    });

    isSecureRequest(
      req,
      res,
      () => {
        // Secure request
        requireMinimumRole(ROLE.SUPER, req, res, () => {
          user.create(
            req.knex,
            { ...req.body, pass: newPass, f_reset: false, f_valid: true },
            (r) => {
              res.status(r.code).json(r.data);
              if (r.code === RESPONSE.CREATED)
                sendValidationEmailTo(req.knex, r.data.email, newPass);
            }
          );
        });
      },
      () => {
        // Anonymous request
        user.create(
          req.knex,
          {
            name: req.body.name,
            email: req.body.email,
            pass: newPass,
            f_reset: true,
            f_valid: false
          },
          (r) => {
            res.status(r.code).json(r.data);
            if (r.code === RESPONSE.CREATED)
              sendValidationEmailTo(req.knex, r.data.email, newPass);
          }
        );
      }
    );
  });
});

router.get('/:uuid', (req, res) => {
  requireInParams(['uuid'], req, res, () => {
    requireMinimumRoleOrSelf({
      uuid: req.params.uuid,
      requiredRole: ROLE.SUPER,
      req,
      res,
      onValid: () => {
        user.read(req.knex, { uuid: req.params.uuid }, (r) =>
          res.status(r.code).json(r.data)
        );
      }
    });
  });
});

router.delete('/:uuid', async (req, res) => {
  requireInParams(['uuid'], req, res, () => {
    const { uuid } = req.params;
    req.knex
      .select()
      .from('users')
      .where({ uuid })
      .then((targetUser) => {
        if (!targetUser) return res.sendStatus(RESPONSE.NOT_FOUND);
        requireMinimumRoleOrSelf({
          uuid,
          requiredRole: ROLE.SUPER,
          req,
          res,
          onValid: async () => {
            user.destroy(req.knex, { uuid }, (r) =>
              res.status(r.code).json(r.data)
            );
          }
        });
      });
  });
});

router.patch('/:uuid', (req, res) => {
  requireInParams(['uuid'], req, res, () => {
    requireMinimumRoleOrSelf({
      uuid: req.params.uuid,
      requiredRole: ROLE.SUPER,
      req,
      res,
      onValid: async () => {
        user.update(req, (r) => res.status(r.code).json(r.data));
      }
    });
  });
});

router.post('/status', (req, res) => {
  requireInBody(['email'], req, res, () => {
    user.status(req.knex, req.body, (r) => res.status(r.code).json(r.data));
  });
});

router.post('/reset', (req, res) => {
  requireInBody(['email'], req, res, () => {
    const email = req.body.email?.toLowerCase();
    user.reset(req.knex, email, (r) => {
      sendValidationEmailTo(req.knex, r.data.email, r.data.new_password);
      delete r.data.new_password;
      return res.status(r.code).json(r.data);
    });
  });
});

router.post('/validate', (req, res) => {
  requireValidPostedValidationToken(req, res, (email) => {
    req
      .knex('users')
      .where({ email })
      .update({ refresh_secret: null, f_valid: 1 })
      .then(() => res.sendStatus(RESPONSE.OK));
  });
});

router.post('/changepass', (req, res) => {
  requireInBody(['email', 'pass'], req, res, () => {
    const { email, pass, newPass, newPassConfirm } = req.body;
    user.changePassword(
      req.knex,
      {
        email,
        pass,
        new_password: newPass,
        new_password_confirm: newPassConfirm
      },
      (response, code) => {
        res.status(code).send(response?.toString());
      }
    );
  });
});

module.exports = router;
