const metadataParser = require('markdown-yaml-metadata-parser');
const templateDir = `${__dirname}/templates`;
const fs = require('fs');

const replace = ({ recipient, template, content, invoker }) => {
  const unsubscribeLink =
    process.env.MAIL_TEMPLATE_UNSUBSCRIBE_LINK.length > 0
      ? `Don't like these emails? <a href="${process.env.MAIL_TEMPLATE_UNSUBSCRIBE_LINK}"
                style="text-decoration: underline; color: #999999; font-size: 12px; text-align: center;">Unsubscribe</a>`
      : '';
  const merged = template
    .replace(/~INVOKER_EMAIL~/g, invoker?.email)
    .replace(/~TO_NAME~/g, recipient.name)
    .replace(/~EVENT_NAME~/g, recipient.eventName)
    .replace(/~TO_EMAIL~/g, recipient.email)
    .replace(/~FROM_NAME~/g, process.env.MAIL_FROM_NAME)
    .replace(/~FROM_EMAIL~/g, process.env.MAIL_FROM_EMAIL)
    .replace(/~COPYRIGHT~/g, process.env.MAIL_TEMPLATE_COPYRIGHT)
    .replace(/~MAILING_ADDRESS~/g, process.env.MAIL_TEMPLATE_MAILING_ADDRESS)
    .replace(/~SERVICE_NAME~/g, process.env.MAIL_TEMPLATE_SERVICE_NAME)
    .replace(/~SERVICE_LINK~/g, process.env.MAIL_TEMPLATE_SERVICE_LINK)
    .replace(/~UNSUBSCRIBE_LINK~/g, unsubscribeLink);
  if (content) {
    return merged
      .replace(/~NEW_PASSWORD~/g, content.newPass.trim())
      .replace(/~VALIDATION_LINK~/g, content.validationLink);
  }
  return merged;
};

module.exports = {
  buildHTML: (props, cb) => {
    const { template, to, name, eventName, content, invoker } = props;
    const templatePath = `${templateDir}/${template}.html`;
    fs.readFile(templatePath, 'utf8', function (e, data) {
      if (e) console.error(e);
      if (!data) console.error(`MISSING: ${templatePath}`);
      const parsed = metadataParser(
        replace({
          recipient: { to, name, eventName },
          template: data,
          invoker
        })
      );
      const { ctaLabel, ctaLink, subject } = parsed.metadata;
      fs.readFile(`${templateDir}/cta.html`, 'utf8', function (e, ctaData) {
        if (e) console.error(e);
        fs.readFile(`${templateDir}/footer.html`, 'utf8', function (e, footer) {
          if (e) console.error(e);
          fs.readFile(`${templateDir}/base.html`, 'utf8', function (e, data) {
            if (e || !data) console.error(e);
            let html = data
              .replace(/~CONTENT_TO_WRAP~/g, parsed.content)
              .replace(/~FOOTER~/g, footer);
            if (ctaLink) {
              const cta = ctaData
                .replace(/~LABEL~/g, ctaLabel)
                .replace(/~LINK~/g, ctaLink);
              html = html.replace(/~CTA~/g, cta);
            } else {
              html = html.replace(/~CTA~/g, '');
            }
            // NOTE: We run replace twice to do a replace on the wrapped content
            html = replace({
              recipient: { to, name },
              template: replace({
                recipient: {
                  to,
                  name,
                  eventName
                },
                template: html,
                invoker
              }),
              content,
              invoker
            });
            const from = process.env.MAIL_FROM_NAME
              ? `${process.env.MAIL_FROM_NAME} ${process.env.MAIL_FROM_EMAIL}`
              : process.env.MAIL_FROM_EMAIL;
            cb({ to, from, subject, text: parsed.plainText, html });
          });
        });
      });
    });
  }
};
