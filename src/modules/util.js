const fs = require('fs-extra');
const path = require('path');
let jwt = require('jsonwebtoken');

module.exports = {
  isDebugMode: () => {
    return (
      process.env.DEBUG_MODE &&
      process.env.DEBUG_MODE.trim().toLowerCase() === 'true'
    );
  },
  environmentOK: () => {
    let fail =
      !process.env.MAIL_MAILTRAP_USER ||
      !process.env.MAIL_MAILTRAP_PASS ||
      !(process.env.MAIL_SENDGRID_API_KEY || process.env.MAIL_MAILGUN_API_KEY);
    !process.env.JWT_SECRET;

    return !fail;
  },

  log: (message) => {
    if (module.exports.isDebugMode()) console.log(message);
  },

  saveJSONToFile: (json, directory, file) => {
    if (typeof json === 'undefined') {
      console.error(`Cannot save ${file} to package, data missing in post.`);
      return;
    }
    let outputFile = fs.createWriteStream(path.join(directory, file), {
      flags: 'w'
    });
    outputFile.write(JSON.stringify(json), () => {
      outputFile.end();
    });
  }
};
