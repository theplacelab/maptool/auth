# Node Server for User support

## API For Server

> 🟠 Requires bearer token which should be the email being operated on (self) or belong to a valid superuser

| METHOD    | ENDPOINT         | BODY                                                                                   |
| --------- | ---------------- | -------------------------------------------------------------------------------------- |
| 🟠 GET    | /send/:TEMPLATE  |
|           |                  |
| 🟠 GET    | /pwd/reset       |
| 🟠 POST   | /pwd/reset       | {password: [NEW PASSWORD], email:[optional, if included must submit with super token]} |
|           |                  |
| 🟠 POST   | /user            | {email:[email],name:'',picture:''}                                                     |
| 🟠 PATCH  | /user            | {email:[email],name:'',picture:''}                                                     |
| 🟠 DELETE | /user            | {email:[email]}                                                                        |
|           |                  |
| 🟢 GET    | /validate/:TOKEN |
| 🟢 POST   | /validate        | {token:[VALIDATION TOKEN],password:[NEW PASSWORD]}                                     |

&nbsp;

## Email Templates

> You can use docker volume mapping to completely replace the templates directory or any files inside it.

Inside the src/templates directory are templates, the name of these correspond to a route, so if you GET to `/send/test` for instance, the contents of `template/test.html` will send. You can add or replace these using docker volume mapping.

Templates are originally based on [this](https://github.com/leemunroe/responsive-html-email-template/blob/master/email-inlined.html) and support the following strings which will be dynamically replaced:

| PLACEHOLDER          | VALUE FROM |
| -------------------- | ---------- |
| `~TO_NAME~`          | token      |
| `~TO_EMAIL~`         | token      |
|                      |            |
| `~NEW_PASWORD~`      | generated  |
| `~VALIDATION_LINK~`  | generated  |
|                      |            |
| `~FROM_NAME~`        | `.env`     |
| `~FROM_EMAIL~`       | `.env`     |
| `~COPYRIGHT~`        | `.env`     |
| `~MAILING_ADDRESS~`  | `.env`     |
| `~UNSUBSCRIBE_LINK~` | `.env`     |
| `~SERVICE_NAME~`     | `.env`     |
| `~SERVICE_LINK~`     | `.env`     |

&nbsp;

## Environment Variables (.env)

```
# TRUE will send email to mailtrap
DEBUG_MODE=TRUE

# Defaults to 8888
PORT=8888

# URL to the Postgres API in front of the user DB
REACT_APP_USER_API=https://example.com/user


# Secret should match user DB
JWT_SECRET=

# Secret for creating validation links
JWT_VALIDATION_SECRET=

# Mailer settings, you should leave either SENDGRID or MAILGUN blank
# (only provide credentials for the one you're using)
MAIL_MAILTRAP_USER=
MAIL_MAILTRAP_PASS=
MAIL_MAILGUN_API_KEY=
MAIL_MAILGUN_DOMAIN=example.com
MAIL_SENDGRID_API_KEY=

# Mail From (may need to match mailer service settings)
MAIL_FROM_NAME="Admin Account"
MAIL_FROM_EMAIL=mail@example.com

# Mail template values, for mailmerge, can leave blank
MAIL_TEMPLATE_SERVICE_NAME="Maptool"
MAIL_TEMPLATE_SERVICE_LINK="http://cnn.com"
MAIL_TEMPLATE_COPYRIGHT='&copy; 2021 - powered by ^.^'
MAIL_TEMPLATE_UNSUBSCRIBE_LINK=''
MAIL_TEMPLATE_MAILING_ADDRESS='This is an automated message from ~FROM_NAME~ (~FROM_EMAIL~), please do not reply. If you did not request this email, you can ignore it.'
```

https://medium.com/@rickhanlonii/understanding-jest-mocks-f0046c68e53c
